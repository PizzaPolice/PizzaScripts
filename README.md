A Collection of personal scripts for KolMafia for use in the browser game Kingdom of Loathing

hccs.ash: 
An extremely reliable but extremely stupid hccs 2 day ascension script based on a route i wrote.
The script is extremely hardcoded to the stuff I have, feel free to modify it to suit your needs.

breakfast.ash:
Simple breakfast script, checks for aftercore, also functions as a post-ascension script

bed.ash:
Aftercore bedtime scripts, If you can not drink the Pickle juice it will just fail. As of now
does not check if you have the equipment that I have.

fantasy.ash:
Farms fantasyrealm using 4 noncombats for rubbees. Assumes you have cemetery/village/mountains unlocked.
and have a pickaxe/monocle/magnifying glass. Uses new age healing crystals and saucestorm + meteorb for combat.

Note: Unless you manage to steal my account, scripts are not "fire and forget". Scripts depend
on various levels of assumptions that hold true on my account but may not on yours. Feel free to
make a pull request or ask me to make changes you would like to see. They are primarily intended as a
baseline or "90% of the way there" script where you modify it to suit your needs better.
