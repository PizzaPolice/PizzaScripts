import <sl_ascend.ash>
import <CONSUME.ash>
void main()
{
  //If not in aftercore, dont do anything
	if(can_interact()) {
    //Check if full drunk and food first
    if (inebriety_limit() > my_inebriety() || my_fullness() < fullness_limit())
    {
      print("Not Full and Drunk, aborting");
      return;
    }
    
    if (my_adventures() > 60) {
      print("Too many adventures, aborting");
      return;
    }
    

    int fights = pvp_attacks_left();
    //put on pvp outfit
    cli_execute("outfit obv");
    //pvp that many times
    cli_execute("pvp " + fights + " flowers 8");

    //Change to rollover outfit
    maximize("adv .5pvp +equip ratskin pajama pants", false);
    //Cast ode
    while (to_int($effect[ode to booze]) < inebriety_limit() - my_inebriety())
    {
      cli_execute("skill ode to booze");
    }

    //Equip stooper and solid shifting time thing
    cli_execute("familiar stooper");
    if (!have_equipped($item[solid shifting time weirdness]))
    {
      cli_execute("equip solid shifting time weirdness");
    }

    //have CONSUME do our diet
    cli_execute("CONSUME.ash NIGHTCAP");
    
    //Zap hamethysts/baconstones as needed
    if (get_property("_zapCount").to_int() == 0)
    {
      cli_execute("zap hamethyst");
    }

    if (get_property("_zapCount").to_int() == 0)
    {
      cli_execute("zap baconstone");
    }
       

    //back to main clan
    cli_execute("/whitelist reddit");
  }

}
