//crappy barf farming script
script "barf.ash"

//imports
import <sl_ascend.ash>
import <CONSUME.ash>

//method declarations
boolean preScriptCheck();
boolean checkSupply(item i, int amount);
string barfCombat(int round, monster opp, string text);


void main()
{

  if (preScriptCheck())
  {
    abort("Pre script check failed, see red text for information");
  }

  //if we don't have a pantogram pants yet, make one
  if (item_amount($item[pantogram pants]) == 0 && !have_equipped($item[pantogram pants]))
  {
    use(1,$item[disassembled clover]);
    //create pantogram, will not create one if we already have one
    pantogramPants($stat[moxie],$element[stench],7,4,11);
  }

  //grab a carpe
  if (get_property("_floundryItemCreated").to_boolean() == false)
  {
    if (!create(1,$item[carpe]))
    {
      print("FAILED TO MAKE CARPE","red");
      abort();
    }
  }

  //buff up like a bloody madman
  if (get_property("_madTeaParty").to_boolean() == false)
  {
    cli_execute("hatter 22");
  }
  cli_execute("concert Winklered");
  if (get_property("_clanFortuneBuffUsed").to_boolean() == false)
  {
    cli_execute("fortune buff meat");
  }
  while (get_property("_poolGames").to_int() < 3)
  {
    cli_execute("pool 1");
  }

  if (get_property("demonSummoned").to_boolean() == false)
  {
    cli_execute("summon 2");
  }
  if (get_property("_jukebox").to_boolean() == false)
  {
    cli_execute("jukebox meat");
  }

  //Grab bastille rewards
  cli_execute("bastille.ash mainstat gesture draftsman");

  //Fuel up our asdon martin
  if (get_fuel() < 1000)
  {
    cli_execute("Gasdon 1000");
  }

  //use our one-day ticket if we haven't already
  if (get_property("stenchAirportAlways").to_boolean() == false && get_property("_stenchAirportToday").to_boolean() == false)
  {
    use(1,$item[one-day ticket to dinseylandfill]);
  }
  //let CONSUME do all the work
  cli_execute("CONSUME.ash ALL");

  //turn in park garbage
  if (get_property("_dinseyGarbageDisposed").to_boolean() == false)
  { 
    visit_url( "place.php?whichplace=airport_stench&intro=1" );
    visit_url( "place.php?whichplace=airport_stench&action=airport3_tunnels" );
    run_choice( 6 );
  }

  //suit up!
  use_familiar($familiar[hobo monkey]);
  maximize("meat +equip carpe  +equip pantogram pants +equip mafia thumb ring +equip cheap sunglasses",false);

  //set our mood (ASSUMES MOOD EXISTS AND IS SET UP CORRECTLY
  //E.G. CHEAP FAM WEIGHT/MEAT BUFFS, SYNTHESIS, ASDON, HOW TO SCAM TOURISTS
  cli_execute("mood barf");

  //farm like a bloody madman!
  print("Prep work done, time to farm!","blue");

  print("Switching Clans and Getting Fax", "blue");
  if(item_amount($item[photocopied monster]) == 0 && get_property("_photocopyUsed") == "false"){
    while(get_clan_id() != 2047000514){
      cli_execute("/whitelist Hunior's Harem");
    }
    cli_execute("fax receive");
    while(get_clan_id() == 2047000514 && item_amount($item[photocopied monster]) == 1 ){
      cli_execute("/whitelist Reddit United");
    }
  }
  if(item_amount($item[photocopied monster]) == 1 && get_property("_photocopyUsed") == "false" ){
    use( 1, $item[ photocopied monster ] );
    run_combat();
    while(in_multi_fight()){
    run_combat();
    }
  }

  //Murderize the embezzler ... again!
  if (!get_property("_cameraUsed").to_boolean() &&
    item_amount($item[shaking 4-d camera]) == 1 &&
    get_property("cameraMonster") == $monster[knob goblin embezzler])
  {
    use(1,$item[shaking 4-d camera]);
  }
  
  //use up NEP free fights
  //Make sure choice adventures rejects quest and takes fight on the noncombat
  if (get_property("neverendingPartyAlways").to_boolean()) {
    while(get_property("_neverendingPartyFreeTurns").to_int() < 10)
    {
      adv1($location[The Neverending Party],-1,"barfCombat");
    }
  }
  
  //while loop for in case we get adventures from the thumb ring
  while(my_adventures() != 0)
  {
    adventure(my_adventures(),$location[Barf Mountain],"barfCombat");
  }

  //Change mood back to nothing
  cli_execute("mood apathetic");

  //Open up daily meat boxes!
  //save a few garbage bags for tomorrow's turn in
  use(item_amount($item[bag of park garbage]) - 5,$item[bag of park garbage]);
  use(item_amount($item[gathered meat-clip]),$item[gathered meat-clip]);
}

boolean preScriptCheck()
{
  boolean error = false;

  //sanity
  if(!can_interact()) {
    error = true;
    print("Not in aftercore","red");
  }

  //Check if full drunk and food first
  if (my_inebriety() != 0 || my_fullness() != 0)
  {
    print("Stomach and Liver not both empty, will skip eating/drinking if necessary");
  }
  //supply checks
  //If able to be found in NPC stores, buy them automatically
  //Else will yell at player to buy them themselves

  //potions
  //nasal spray
  if (!checkSupply($item[knob goblin nasal spray],100)) 
  {
    print("Buying Knob Goblin nasal sprays","red");
    buy(100 - item_amount($item[knob goblin nasal spray]),$item[knob goblin nasal spray]); 
  }

  //pet buffing spray
  if (!checkSupply($item[knob goblin pet-buffing spray],100) )
  {
    print("Buying Knob Goblin pet-buffing sprays","red");
    buy(100 - item_amount($item[knob goblin pet-buffing spray]),$item[knob goblin pet-buffing spray]); 
  }

  error = error | !checkSupply($item[how to avoid scams],25);

  //demon name supplies
  error = error | !checkSupply($item[scroll of ancient forbidden unspeakable evil],1);
  error = error | !checkSupply($item[thin black candle],3);

  //pantogram supplies 
  error = error | !checkSupply($item[porquoise],1);
  error = error | !checkSupply($item[disassembled clover],1);

  //Tickets for dinseylandfill
  error = error | !checkSupply($item[one-day ticket to dinseylandfill],1);

  //misc
  error = error | !checkSupply($item[4-d camera],1);

  //return if we have any errors
  return error;
}

//Checks if we have enough of a given item, print an error msg if we don't
boolean checkSupply(item i, int amount)
{
  //if we have enough, return error and print
  if (item_amount(i) < amount) 
  {
    print("Need " + amount + " " + i  + ", only have: " + item_amount(i),"red");
    return false;
  }
  //else we have enough
  return true;
}

string barfCombat(int round, monster opp, string text)
{
  //All of the NEP monsters in a list
  monster [int] nepMonsters = get_monsters($location[The Neverending Party]);
  boolean isNEPMonster = false;
  
  foreach mon in nepMonsters
  {
    if (to_monster(mon) == opp) {
      isNEPMonster = true;
      print(isNEPMonster);
    }
  }
  if (have_skill($skill[sing along]) && round == 0)
  {
    return "skill sing along";
  }
  else if (opp == $monster[garbage tourist] && have_effect($effect[on the trail]) == 0)
  {
    return "skill transcendent olfaction";
  }
  else if (opp == $monster[knob goblin embezzler] && get_property("_cameraUsed").to_boolean() == false)
  {
    return "use 4-d camera";
  }
  //don't use up punches on stuff like NEP free fights
  else if (have_skill($skill[shattering punch]) &&
    get_property("_shatteringPunchUsed").to_int() < 3 &&
    opp == $monster[garbage tourist])
  {
    return "skill shattering punch";
  }
  else if (opp == $monster[garbage tourist] ||
    opp == $monster[angry tourist] ||
    opp == $monster[horrible tourist family])
  {
    return "attack with weapon";
  }
  //NEP fights
  else
  {
    return "skill saucegeyser";
  }

}
