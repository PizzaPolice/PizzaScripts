//Shitty fantasyrealm farming script
script "fantasy.ash"

//method declarations
string combat(int round, monster opp, string text);
boolean preScriptCheck();

string combat(int round, monster opp, string text) 
{
  //new age healing spam on the cursed villager
  if (opp == $monster[cursed villager] ) 
  {
    //if funkslinging, just funksling healing and hurting
    if (have_skill($skill[Ambidextrous Funkslinging]))
    {
      return "item new age healing, new age hurting";
    }
    //else alternate
    else
    {
      if (round % 2 == 0)
      {
        return "item new age healing";
      }
      else
      {
        return "item new age hurting";
      }
    }
  }
  else if (opp == $monster[fantasy ourk])
  {
      return "skill saucestorm";
    
  }
  else if (opp == $monster[rubber bat])
  {
    //if funkslinging, just funksling healing and hurting
    if (have_skill($skill[Ambidextrous Funkslinging]))
    {
      return "item new age healing, new age hurting";
    }
    //else alternate
    else
    {
      if (round % 2 == 0)
      {
        return "item new age healing";
      }
      else
      {
        return "item new age hurting";
      }
    }
  }
  else if (opp == $monster[spooky ghost])
  {
    if (have_skill($skill[shattering punch]) && get_property("_shatteringPunchUsed") < 3)
    {
      return "skill shattering punch";
    }
    else
    {
      return "skill saucestorm";
    }
  }
  
  
  return "skill CLEESH";
}


boolean preScriptCheck()
{
  boolean error = false;
  if (get_property("frVillageUnlocked") == False)
  {
    print("You do not have the Fantasy Realm Village Unlocked","red");
    error = true;
  }
  if (get_property("frMountainsUnlocked") == False)
  {
    print("You do not have the Fantasy Realm Mountains Unlocked","red");
    error = true;
  }
  if (get_property("frCemetaryUnlocked") == False)
  {
    print("You do not have the Fantasy Realm Cemetery Unlocked","red");
    error = true;
  }
  
  if (my_adventures() < 25)
  {
    print("You have less than 25 adventure remaining","red");
    error = true;
  }
  
  if (item_amount($item[FantasyRealm key]) < 1)
  {
    print("You need a FantasyRealm key to open the key in the foreboding cave","red");
    error = true;
  }
  
  if (item_amount($item[new age healing crystal]) < 30)
  {
    print("You have less than 30 new age healing crystals. While you probably will not use them all, it's best to have a few extras","red");
    error = true;
  }
  
  if (item_amount($item[LyleCo premium rope]) < 1)
  {
    print("You do not have LyleCo Rope, will take the buff instead of diving down the well in the village","red");
    error = true;
  }
  
    if (item_amount($item[LyleCo premium rope]) < 1)
  {
    print("You do not have LyleCo Rope, will take the buff instead of diving down the well in the village","red");
    error = true;
  }
    
    if (item_amount($item[LyleCo premium pickaxe]) < 1)
  {
    print("You do not have LyleCo Pickaxe, It is needed to dig for graves in the cemetery","red");
    error = true;
  }
  
  return error;
}

//main method
void main()
{
  //Village -> mountains -> cave -> cemetary is the route taking all the rubbe noncombats
  
  //Do our pre script check
  boolean warning = preScriptCheck();
  
  //display warning to script user if needed
  if (warning)
  {
    print("Pre Script Check has found something wrong, if you think everything is fine then the script will start in 15 seconds","red");
    wait(15);
  }

  //Grab the G.E.M
  print("Grabbing G.E.M.", "blue");
  visit_url("place.php?whichplace=realm_fantasy&action=fr_initcenter", false);
  run_choice(2); //mage

  //remove out familiar
  print("Removing familiar", "blue");
  use_familiar($familiar[none]);
  
  print("Equipping G.E.M. and the rubbee increasing equipment","blue");
    //equip it
  equip($slot[acc1],$item[FantasyRealm G. E. M.]);
  equip($item[LyleCo premium magnifying glass]);
  equip($slot[acc2],$item[LyleCo premium monocle]);
  equip($item[meteorb]);
  //set all the noncombat choices
  
  //well if have rope, else buff for village
  if (item_amount($item[LyleCo premium rope]) >= 1)
  {
    print("We have rope, diving in well for rubbees","blue");
    set_property("choiceAdventure1285", 5);
  }
  else
  {
    print("We dont have rope, taking the rubbee buff","blue");
    set_property("choiceAdventure1285", 4);
  }
  //Towering mountains, go to the foreboding cave
  set_property("choiceAdventure1282", 2);
  
  //Open the chest in the cave
  set_property("choiceAdventure1289", 1);
    
  //Rob graves in the cemetery
  set_property("choiceAdventure1286", 4);
  
  //Run through looting EVERYTHING!
  adventure(6,$location[The Cursed Village],"combat");
  adventure(6,$location[The Towering Mountains],"combat");
  adventure(6,$location[The Foreboding Cave],"combat");
  adventure(6,$location[The Sprawling Cemetery],"combat");
}
