//Shitty HCCS script for owner202
script "hccs.ash"

//stealing all the clan functions
import <sl_ascend.ash>

//method declarations
void boozeShower(int turns);
void burnMP();
void day1();
void day2();
void day1setup();
void breakfast();
void doTask(int quest);
void refuel();
void numberology();
void main()
{
  //Familiar you want to do your 100% run on, SET THIS BEFORE EVERY RUN
  familiar ToTour = $familiar[Syncopated Turtle];

  //Check for HCCS path, if not, make go away forever	
  if(my_path() != "Community Service" || can_interact())
  {
    abort("Not in a HCCS run");
  }

  //set our script to the hccs script (YELLOW RAY EVERYTHING!)
  cli_execute("ccs hccs");

  //cast numberology for the day
  numberology();
  
  //use the bloody familiar
  use_familiar(ToTour);

  //switch reddit united for full VIP lounge
  cli_execute("/whitelist reddit");

  //setup if day 1
  day1setup();

  //do daily things
  breakfast();

  if (my_daycount() == 1)
  {
    day1();
  }
  else if (my_daycount() == 2)
  {
    day2();
  }
  else
  {
    print("We're past day 2, something went horribly wrong","red");
  }
}

void day1()
{ 
  int	adv = my_adventures();

  if (item_amount($item[pantogram pants]) == 0)
  {
    //grab our day 1 pantogram pants
    pantogramPants($stat[Mysticality],$element[hot],2,1,2);
  }
  equip($item[pantogram pants]);

  if (get_property("tomeSummons").to_int() == 0)
  {
    //get our day 1 clip art summons
    print("Summoning day 1 clip art","blue");
    cli_execute("make unbearable light");
    cli_execute("make time halo");
    cli_execute("make ultrafondue");
  }
  //Murdering some poor booze giant
  if (!get_property("_photocopyUsed").to_boolean())
  {
    //so we dont die horribly
    cli_execute("fold deceased crimbo tree"); 
    equip($item[deceased crimbo tree]);

    //murderize the booze giant
    if (item_amount($item[unbearable light]) >= 1)
    {
      boolean faxResult = faxbot($monster[booze giant]);
      //sanity check
      if (item_amount($item[photocopied monster]) == 1 && 
          get_property("photocopyMonster") == $monster[booze giant] &&
          faxResult == true)
      {
        //murderizing time!, make sure CCS is set
        use(1, $item[photocopied monster]);       
      }
      else
      {
        abort("Fax failed");
      }
    }
    else
    {
      abort("Did not have YR for killing booze giant fax");
    }
  }

  //buy our cocktailcrafting kit
  if (!get_property("hasCocktailKit").to_boolean() && item_amount($item[queue du coq cocktailcrafting kit]) == 0)
  {
    if (!buy(1, $item[Queue Du Coq cocktailcrafting kit]))
    {
      abort("Failed to buy cocktailcrafting kit");
    }
    use(1,$item[Queue du coq cocktailcrafting kit]);
  }

  if (item_amount($item[peppermint sprout]) == 3)
  {
    //make our peppermint drinks
    cli_execute("make 3 peppermint twist");
  }
  if(have_skill($skill[Perfect Freeze]))
  {
    use_skill(1 ,$skill[Perfect Freeze]);
  } 

  if (item_amount($item[peppermint twist]) == 3 && item_amount($item[perfect ice cube]) == 1)
  {
    craft("cocktail", 1, $item[peppermint twist],$item[bottle of whiskey]);
    craft("cocktail", 1, $item[peppermint twist],$item[bottle of vodka]);
    craft("cocktail", 1, $item[peppermint twist],$item[bottle of rum]);
    craft("cocktail", 1, $item[perfect ice cube],$item[boxed wine]);
  }

  if (my_inebriety() == 0)
  {
    //ode up for day 1
    boozeShower(5);
    //first 2 drinks
    drink(1,$item[Mint Yulep]);
    drink(1,$item[Vodka Matryoshka]);
  }
  //coil wire
  doTask(11);

  //use the ten percent bonus
  if (item_amount($item[a ten-percent bonus]) > 0) 
    use(1, $item[a ten-percent bonus]);

  if (my_inebriety() == 4)
  {
    //drink the rest of our booze including our perfect drink
    boozeShower(10);
    drink(1,$item[Perfect mimosa]); 
    drink(1,$item[Crimbojito]);
    drink(5,$item[astral pilsner]);
  }

  //buff up for sausage
  print("Buffing up for sausage","blue");
  cli_execute("pool 2");

  //do sausage
  doTask(7);

  //hermit clovers
  hermit(999, $item[ten-leaf clover]);

  //get chewing gum
  while (item_amount($item[disassembled clover]) > 0) 
  {
    use(1, $item[disassembled clover]);
    visit_url("adventure.php?snarfblat=45");
    use(1, $item[pack of chewing gum]);
  }

  //time for eatin
  if (my_fullness() == 0)
  {
    cli_execute("genie effect got milk");
    eat(1,$item[sausage without a cause]);
    eat(1,$item[ultrafondue]);
    eatFancyDog("one with everything");
  }

  print("Buffing hot res","blue");
  //buff up for steam tunnels
  if (item_amount($item[pocket wish]) == 2)
  {
    cli_execute("genie effect fireproof lips");
    cli_execute("synthesize hot");
    equip($item[pantogram pants]); 
    use_skill(1 ,$skill[elemental saucesphere]);

    if (item_amount($item[psychic's amulet]) > 0)
    {	
      equip($slot[acc2], $item[psychic's amulet]);
    }  
  }

  //hot res test
  doTask(10);

  //night cap time!
  if (my_inebriety() == 14)
  {
    boozeShower(10);
    overdrink(1,$item[vintage smart drink]);
  }  

  //buff up mus for tomorrow's hp + mus test
  zataraSeaside("gunther");

  //pajamas
  cli_execute("fold garbage shirt");
  equip($item[time halo]);
  equip($item[makeshift garbage shirt]);
  //take off off-hand and main-hand for time halo to work
  equip($slot[weapon],$item[none]);
  equip($slot[off-hand],$item[none]);

  //burn any spare mp on stuff like cold one
  burnMP();
}



void day2()
{
  int	adv = my_adventures();

  if (item_amount($item[pantogram pants]) == 0)
  {
    //grab our day 2 pantogram pants
    //Mus/hot/hp/noncom/wepDmg
    pantogramPants($stat[Muscle],$element[hot],1,1,1);
  }
  equip($item[pantogram pants]);

  if (get_property("tomeSummons").to_int() == 0)
  {
    //get our day 2 clip art summons
    print("Summoning day 2 clip art","blue");
    cli_execute("make unbearable light");
    cli_execute("make borrowed time");
    cli_execute("make ultrafondue");

    //Buy our range efor the day
    if (!buy(1, $item[Dramatic&trade; range]))
    {
      abort("Failed to buy oven");
    }
    use(1, $item[Dramatic&trade; range]);
  }

  //turn in to twists for synthesis fodder
  if (item_amount($item[peppermint sprout]) == 3)
  {
    //twists for synthesis fodder
    cli_execute("make 3 peppermint twist");
  }

  print("Buffing up for the HP test","blue");
  //Buff up for our Hp/mus tests
  if (item_amount($item[pocket wish]) == 4)
  {
    cli_execute("genie effect preemptive medicine");
    cli_execute("synthesize hardy");
    cli_execute("synthesize strong");
    cli_execute("hatter 12");

    cli_execute("fold wad of used tape");
    equip($item[wad of used tape]);
  }

  //HP test
  doTask(1);

  //muscle task
  doTask(2);

  //murder our dairy goat and make milk of magnesium
  if (!get_property("_photocopyUsed").to_boolean())
  {
    //so we dont die horribly
    cli_execute("fold deceased crimbo tree"); 
    equip($item[deceased crimbo tree]);

    //murderize the goat
    if (item_amount($item[unbearable light]) >= 1)
    {
      boolean faxResult = faxbot($monster[dairy goat]);
      //sanity check
      if (item_amount($item[photocopied monster]) == 1 && 
          get_property("photocopyMonster") == $monster[dairy goat] &&
          faxResult == true)
      {
        //murderizing time!, make sure CCS is set
        use(1, $item[photocopied monster]);
      }
      else
      {
        abort("Fax failed");
      }
    }
    else
    {
      abort("Did not have YR for killing booze giant fax");
    }
  }

  if (item_amount($item[glass of goat's milk]) == 1)
  {
  if(have_skill($skill[Advanced Saucecrafting]))
  {
    use_skill(1 ,$skill[Advanced Saucecrafting]);
  }
    //make our milk of magnesium
    craft("cook",1,$item[glass of goat's milk],$item[scrumptious reagent]);
  }

  //buff up for gazelle murder!
  if (item_amount($item[weird gazelle steak]) == 0 && my_fullness() == 0)
  {
    cli_execute("fold broken champagne bottle");
    equip($item[broken champagne bottle]);
    cli_execute("pool 1");
  }

  //time to murder gazelles!

  doTask(6);

  //Time to eat!
  if (my_fullness() == 0)
  {
    use(1,$item[milk of magnesium]);
    eat(1,$item[weird gazelle steak]);
    eat(1,$item[ultrafondue]);
    eatFancyDog("one with everything");
  }

  //Buff up for margaritas
  if(item_amount($item[pocket wish]) == 3)
  {
    //skills
    use_skill(1 ,$skill[fat leon's phat loot lyric]);
    
    use_skill(1 ,$skill[singer's faithful ocelot]);
    use_skill(1 ,$skill[Steely-Eyed Squint]);

    //abort of squint wasn't cast
    if (have_effect($effect[Steely-Eyed Squint]) != 1)
    {
      abort("Squint failed to cast");
    }

    if (my_meat() > 1000 && have_effect($effect[driving observantly]) == 0)
    {
      refuel();
      cli_execute("asdonmartin drive observantly");
    }

    //synthesis item
    cli_execute("synthesize collection");

    //wish for infernal thirst and frosty
    cli_execute("genie effect infernal thirst");
    cli_execute("genie effect frosty");

    //consult hagnk
    zataraSeaside("hagnk");

    //vip pool table
    cli_execute("pool 3");


    //wad of used tape
    cli_execute("fold wad of used tape");
    equip($item[wad of used tape]);

    //TODO IF NEEDED ASDON MARTIN STUFFS
  }

  //make margaritas
  doTask(9);

  if (my_inebriety() == 0)
  {
    //It's drinking time!
    boozeShower(15);

    //make perfect drink
    if(have_skill($skill[Perfect Freeze]) && (my_mp() >= mp_cost($skill[Perfect Freeze])))
    {
      use_skill(1 ,$skill[Perfect Freeze]);
    }
    craft("cocktail",1,$item[perfect ice cube],$item[bottle of gin]);

    //actual drinkin time
    drink(1,$item[perfect negroni]);
    drink(1,$item[astral pilsner]);
    //TODO BUCKET OF WINE IF MORE MONEY FOR US
    drink(1,$item[emergency margarita]); 
  }

  //if familiar weight test not done yet, buff up for it and do it
  if (item_amount($item[squeaky toy rose]) == 0)
  {
    //vip pool
    cli_execute("pool 1");
    use_skill(1,$skill[leash of linguini]);
    use_skill(1,$skill[empathy of the newt]);
  }

  //familiar weight test
  doTask(5);

  if (item_amount($item[bag of grain]) == 1)
  {
    //Mys buffing time
    use(1,$item[bag of grain]);
  }
  doTask(3);

  if (item_amount($item[pocket maze]) == 1)
  {
    //Mox buffing time
    use(1,$item[pocket maze]);
  }
  doTask(4);

  //buff up for noncombat
  if (item_amount($item[shady shades]) == 1)
  {
    //equip pantogram pants
    equip($item[pantogram pants]);

    if (my_meat() > 1000 && have_effect($effect[driving stealthily]) == 0)
    {
      refuel();
      cli_execute("asdonmartin drive stealthily");
    }
    //wish for disquiet riot
    cli_execute("genie effect disquiet riot");

    //Sonata + smooth movements
    use_skill(1,$skill[smooth movement]);
    use_skill(1,$skill[the sonata of sneakiness]);
    //swim sprints
    cli_execute("swim sprints");
    //use our HCCS rewards
    use(1,$item[shady shades]);

    use(1,$item[squeaky toy rose]);
  }

  doTask(8);


  print("Community Service Run Done!","green");

}

//Does the given HCCS task
void doTask(int quest)
{
  //if not valid
  if (quest < 1 || quest > 11)
  {
    print("Tried to do a Community Service task that does not exist, aborting","red");
    abort("");
  }

  //helpful debug output is helpful
  if (quest == 1)
  {
    print("Doing Task: Donate Blood", "green");
  }
  else if (quest == 2)
  {
    print("Doing Task: Feed The Children (But Not Too Much)", "green");
  }
  else if (quest == 3)
  {
    print("Doing Task: Build Playground Mazes", "green");
  }
  else if (quest == 4)
  {
    print("Doing Task: Feed Conspirators", "green");
  }
  else if (quest == 5)
  {
    print("Doing Task: Breed More Collies", "green");
  }
  else if (quest == 6)
  {
    print("Doing Task: Reduce Gazelle Population", "green");
  }
  else if (quest == 7)
  {
    print("Doing Task: Make Sausage", "green");
  }
  else if (quest == 8)
  {
    print("Doing Task: Be a Living Statue", "green");
  }
  else if (quest == 9)
  {
    print("Doing Task: Make Margaritas", "green");
  }
  else if (quest == 10)
  {
    print("Doing Task: Clean Steam Tunnels", "green");
  }
  else if (quest == 11)
  {
    print("Doing Task: Coiling Wire", "green");
  }

  //keep track of how many adventures we used
  int adv = my_adventures();

  //do the task
  visit_url("council.php");
  run_choice(quest);

  adv = adv - my_adventures();
  print("Used " + adv +  " Adventures", "green");
}

//cast daily spells before mp regen from a task
void burnMP()
{
  print("Using up mp", "blue");	

  if(have_skill($skill[Summon crimbo candy]) && (my_mp() >= mp_cost($skill[Summon crimbo candy])))
  {
    use_skill(1 ,$skill[Summon crimbo candy]);
  } 
  if(have_skill($skill[Perfect Freeze]) && (my_mp() >= mp_cost($skill[Perfect Freeze])))
  {
    use_skill(1 ,$skill[Perfect Freeze]);
  } 
  if(have_skill($skill[Advanced Saucecrafting]) && (my_mp() >= mp_cost($skill[Advanced Saucecrafting])))
  {
    use_skill(1 ,$skill[Advanced Saucecrafting]);
  }
  if(have_skill($skill[Grab a Cold One]) && (my_mp() >= mp_cost($skill[Grab a Cold One])))
  {
    use_skill(1 ,$skill[Grab a Cold One]);
  }
  if(have_skill($skill[Spaghetti Breakfast]) && (my_mp() >= mp_cost($skill[Spaghetti Breakfast])))
  {
    use_skill(1 ,$skill[Spaghetti Breakfast]);
  }
  if(have_skill($skill[Advanced Cocktailcrafting]) && (my_mp() >= mp_cost($skill[Advanced Cocktailcrafting])))
  {
    use_skill(1 ,$skill[Advanced Cocktailcrafting]);
  }
  if(have_skill($skill[Pastamastery]) && (my_mp() >= mp_cost($skill[Pastamastery])))
  {
    use_skill(1 ,$skill[Pastamastery]);
  }
  if(have_skill($skill[Lunch Break]) && (my_mp() >= mp_cost($skill[Lunch Break])))
  {
    use_skill(1 ,$skill[Lunch Break]);
  }  
  if(have_skill($skill[Summon Carrot]) && (my_mp() >= mp_cost($skill[Summon Carrot])))
  {
    use_skill(1 ,$skill[Summon Carrot]);
  }  
}


//Casts ode, then showers and casts ode more
void boozeShower(int turns)
{
  print("Trying to get " + turns + " turns of Ode to Booze","blue");
  while (my_mp() >= mp_cost($skill[the ode to booze]) && have_effect($effect[ode to booze]) < turns)
  {
    use_skill(1, $skill[The Ode to booze]);
  }

  if (have_effect($effect[ode to booze]) < turns)
  {
    cli_execute("shower hot");
  }
  while (have_effect($effect[ode to booze]) < turns)
  {
    use_skill(1, $skill[the ode to booze]);
  }
  print("Got " + have_effect($effect[ode to booze]) + " turns of ode","blue");
}

void breakfast()
{ 
  //Do our daily clan consults with mr. cheesefax
  while (get_property("_clanFortuneConsultUses").to_int() < 3)
  {
    zataraClanmate("");
  }
  //summon crimbo candy
  if(have_skill($skill[summon crimbo candy]))
  {
    use_skill(1 ,$skill[summon crimbo candy]);
  }

  //harvest garden
  visit_url("campground.php?action=garden");

  //dive in vip swimming pool
  visit_url("clan_viplounge.php?preaction=goswimming&subaction=screwaround&whichfloor=2&sumbit=Cannonball!",true);
  visit_url("choice.php?whichchoice=585&pwd=" + my_hash() + "&option=1&action=flip&sumbit=Handstand",true);
  visit_url("choice.php?whichchoice=585&pwd=" + my_hash() + "&option=1&action=treasure&sumbit=Dive for Treasure",true);
  visit_url("choice.php?whichchoice=585&pwd=" + my_hash() + "&option=1&action=leave&sumbit=Get Out",true);

  //crimbo tree and looking glass
  visit_url("clan_viplounge.php?action=crimbotree&whichfloor=2");
  visit_url("clan_viplounge.php?action=lookingglass&whichfloor=2");

  //vip claw
  visit_url("clan_viplounge.php?action=klaw");
  visit_url("clan_viplounge.php?action=klaw");
  visit_url("clan_viplounge.php?action=klaw");

  //normal claw
  visit_url("clan_rumpus.php?action=click&spot=3&furni=3");
  visit_url("clan_rumpus.php?action=click&spot=3&furni=3");
  visit_url("clan_rumpus.php?action=click&spot=3&furni=3");


  //sell all the moxie weeds
  autosell(item_amount($item[moxie weed]), $item[moxie weed]);

  //grab our pocket wishes
  while (get_property("_genieWishesUsed").to_int() < 3)
  {
    cli_execute("genie item pocket");
  }
}

//beginning of run setup
//stolen from ILoath's HCCS script
void day1setup()
{
  if (my_daycount() == 1)
  {

    visit_url("tutorial.php?action=toot", false);

    visit_url("council.php");
    visit_url("guild.php?place=challenge");

    if (item_amount($item[astral six-pack]) > 0) 
      use(1, $item[astral six-pack]);
    else print("You do not have a astral six-pack.", "red");

    if (item_amount($item[letter from King Ralph XI]) > 0) 
      use(1, $item[letter from King Ralph XI]);
    else print("You do not have a letter from King Ralph XI.", "red");

    if (item_amount($item[pork elf goodies sack]) > 0) 
      use(1, $item[pork elf goodies sack]);
    else print("You do not have a pork elf goodies sack.", "red");

    foreach stone in $items[hamethyst,baconstone,porquoise]
      autosell(item_amount(stone), stone);

    if (item_amount($item[toy accordion]) < 1) 
      buy(1, $item[toy accordion]);

    while (item_amount($item[saucepan]) < 1) 
    {
      buy(1, $item[chewing gum on a string]);
      use(1, $item[chewing gum on a string]);
    }
    while (item_amount($item[turtle totem]) < 1) 
    {
      buy(1, $item[chewing gum on a string]);
      use(1, $item[chewing gum on a string]);
    }

    //fantasyland only
    if(get_property("frAlways") == True)
    {
      print("FANTASYLAND WIZARD HAT", "red");
      visit_url("place.php?whichplace=realm_fantasy&action=fr_initcenter", false);
      run_choice(2); //mage
      if (item_amount($item[FantasyRealm Mage's Hat]) > 0) 
      {
        equip($item[FantasyRealm Mage's Hat]);
      }
    }		


    //break pvp stone
    visit_url("peevpee.php?action=smashstone&confirm=on");

    //make bitchin' meatcar
    if (item_amount($item[bitchin' meatcar]) < 1)
    {
      cli_execute("make bitchin");
    }
  }

}

//fuels asdon martin to >37 with soda bread
void refuel()
{
  while (get_fuel() < 37)
  {
    cli_execute("make 1 soda bread");
    cli_execute("asdonmartin fuel 1 soda bread");
  }
}

//casts the daily numberology casts
void numberology()
{
  //day 1, mainstats and 1400 meat (moxie weed)
  //day 2, 2800 meat (double moxie weeds)

  if (get_property("_universeCalculated").to_int() == 0)
  {
    print("Attempting to cast numberology","blue");
    if (my_daycount() == 1)
    {
      cli_execute("numberology 89");
      cli_execute("numberology 14");
    }
    else if (my_daycount() == 2)
    {
      cli_execute("numberology 14");
      cli_execute("numberology 14");
    }
  }


  if (get_property("_universeCalculated").to_int() != 2)
  {
    abort("Failed to cast numberology");
  }
}
