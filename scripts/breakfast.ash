//SUPER DOOPER SECRET PROPERTY OF owner202 NO PEEKING

//zataraClanmante function
import <sl_ascend.ash>


/*
 * Creates buckets of wine with remaining clip art charges
 */
void clip_art() 
{
  //While has clip art charges, has the skill, and can interact with mall 
  int tomeLeft = 3 - get_property("_clipartSummons").to_int();
  print("Using Clip Art Tome");
  print("Creating " + tomeLeft + " Bucket(s) of wine");
  while(tomeLeft >= 1 && have_skill($skill[Summon Clip Art]))
  {
    tomeLeft = tomeLeft - 1;
    cli_execute("create bucket of wine");
  }
}

/*
 * Consumes meteorite-ade to daily limit, if not enough or hippy stone is unbroken
 * print information
 */
int meteorite_ade()
{
  if (!hippy_stone_broken())
  {
    print("Hippy Stone not broken, aborting drinking metorite-ades","red");
    return -1;
  }
  int meteoradeLeft = 3 - get_property("_meteoriteAdesUsed").to_int();
  int meteorStock = item_amount($item[meteorite-ade]);
  if (meteorStock < meteoradeLeft)
  {
    print("Not enough meteorite-ade to fill to daily limit, BUY MORE PLEASE","red");
    return -1;
  }

  while (meteoradeLeft > 0)
  {
    meteoradeLeft --;
    cli_execute("use meteorite-ade");
  }
  meteorStock -= 3;
  print (meteorStock + " Bottle of Meteorite-ade Left, Buy more if needed","blue");
  return 0;
}
void main() 
{
  //Do clan consults with cheesefax
  while (get_property("_clanFortuneConsultUses").to_int() < 3)
  {
    zataraClanmate("");//empty string is because of argument, it is ignored
  }
  //only do aftercore daily stuff if actually in aftercore
  if(can_interact()) {

    //Pull from hagnks first
    cli_execute("pull all");
    //Execute breakfast in case of prism break
    cli_execute("breakfast");

    clip_art();
    meteorite_ade();


    //Replace stooper with slimeling cuz stooper isn't really useful outside of nightcap
    if (my_effective_familiar().to_int() == $familiar[stooper].to_int() && inebriety_limit() > my_inebriety())
    {
      cli_execute("familiar slimeling");
    }
  }

}
